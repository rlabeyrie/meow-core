/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef CARTO_HH_
# define CARTO_HH_

# include <deque>
# include <stack>

# include "cell.hh"
# include "point.hh"

class Way; // forward dec

/// \brief Used in Cell to order segments by distance to the robot.
/// \details The new ends of the open lines are reconnected
/// differently if they are borders of the unknown world, according to
/// these distances.
class Segment
{
public:
  float lambda;
  Point* prev;
  Point* next;

  void debug () const
  {
    std::cerr << "[" << prev << " ; " << next << "] @ " << lambda << std::endl;
  }
};

/// \brief Used in Cell to order segments by distance to the robot.
/// \details The new ends of the open lines are reconnected
/// differently if they are borders of the unknown world, according to
/// these distances.
class SegmentOrder
{
public:
  bool operator() (const Segment& lhs, const Segment& rhs) const
  {
    return (lhs.lambda > rhs.lambda);
  }
};

/// \brief The class called to use the mapping module (cartography)
/// \details It creates an extendable grid, in the cells of which it can
/// store the points seen around the robot (with their absolute positions),
/// and those which make the border of the never-visited world.
class Carto
{
public:
  /// \param decentStep is a relevant cell width for the grid on the map
  /// (e.g.: 20cm in a room, 20m in a town... in the unity of all distances)
  Carto (float decentStep); // param: PROBABLY SOON OBSOLETE!
  ~Carto ();

  /// Make the robot move forward on a distance `distance'.
  void move (float distance);

  /// Turn the robot of an angle `angle' in radians.
  void turn (float angle);

  /// Should be called if a directional sensor detects the first
  /// obstacle to the distance `distance'
  void detect (float distance);

  /// Should be called if a directional sensor doesn't detect any
  /// obstacle, knowing that its visibility is limited to `distance'.
  void nodetect (float distance, const bool objectDetected = false);

  /// Display the map in a PGM.
  void display (const char* filename);

  /// Useful to write the map in an SDL surface.
  /// \params `img' is a `size'x`size' table. `magnification' is a scale factor.
  void display_magnify (unsigned* img, int field_size,
			int offset_x, int offset_y, int magnify = 1);
  /// Useful to write the map in an SDL surface.
  /// \params `img' is a `size'x`size' table. `reduction' is a scale factor.
  void display_reduce (unsigned* img, int display_size,
		       int offset_x, int offset_y, int reduce = 1);

  /// Points of the same date than the map will not be considered.
  /// \return the date of the map which is incremented at each update
  date_t date ();

  /// A new point is added to its cell by `Carto', which updates the list
  /// of points containing the point to add.
  void add (Point* pt);

  /// A new point is made removable for its cell by `Carto'.
  void remove (Point* pt);

  float posX ();
  float posY ();
  float angle ();

  float viewX ();
  float viewY ();
  float viewNorm ();

  /// \return The cell (cellx, celly) (created if necessary).
  Cell* cell (int cellx, int celly, bool create = true);

  /// \return The cell which should contain the point `pt'
  Cell* findCell (Point* pt);

  /// \return The current cell
  Cell* currentCell ();

  void addBorderSegment (float lambda, Point* prev, Point* next);

  void addInVisitedSegment (float lambda, Point* prev, Point* next);

  /// \return The maximum extension authorized to remove a line.
  float epsilon ();

  /// \return The step used to scale the grid on the map
  float getStep ();

  void initCellCover (Cell* cell, Point* first);

  bool isCurrent (Cell* cell);

  void seeNext ();

  Point* nextToSee ();

  float getAperture ();

  void drawSegments (unsigned* img, int display_size, int x1, int y1,
		     int x2, int y2, unsigned col);

  /// Way to use by the points
  Way* pointWay;

  Point* previouslySeen;

private:
  Point* nextToSee_;
  std::stack<Cell*> trashCells_;
  std::stack<Point*> trashPoints_;

  // Global buffers storing the crossed segments, opened or kept ones.
  std::priority_queue<Segment, std::vector<Segment>, SegmentOrder> borders_;
  std::priority_queue<Segment, std::vector<Segment>, SegmentOrder> invisited_;

  // Open the segment [from ; from->prevOnLine] if toNext is true,
  // open the segment [from ; from->nextOnLine] otherwise.
  // Must be called twice: once for each end of the segment.
  // \param lambda is the value returned by `testNeighbor' method.
  // \return The point (`from' or a new one) which is the end of the opening.
  Point* open (Point* from, bool toNext, float lambda);

  // bool purgeKept ();

  void emptyTrash ();

  // Position of the robot (starts from zero in its relative map).
  float posx_;
  float posy_;
  float angle_; // FIXME: differenciate view angle and move angle
  bool moved_;

  float viewx_;
  float viewy_;
  float distance_;

  bool isWaitingPoint_;
  // last points created
  Segment lastWritten_;
  // nearest segment among the last ones which have been crossed and kept.
  Segment lastSeen_;

  // xpositive_map_ and xnegative_map_ represent an abstract relative Grid.
  // For all k >= 0 and n >= 0:
  // | xpositive_map_[2*k][n] = Grid[k][+_origins_[k]+n]
  // | xnegative_map_[2*k][n] = Grid[-k-1][-_origins_[k]+n]
  // | xpositive_map_[2*k+1][n] = Grid[k][+_origins_[k]-n-1]
  // | xnegative_map_[2*k+1][n] = Grid[-k-1][-_origins_[k]-n-1]
  std::deque< std::deque<Cell*> > xpositive_map_;
  std::deque< std::deque<Cell*> > xnegative_map_;
  std::deque<int> xpositive_origins_;
  std::deque<int> xnegative_origins_;

  // A step (cell width) to scale a relevant grid on the map.
  float step_;

  float oldx_;
  float oldy_;

  // Way looked by the robot
  Way* way_;

  // Expected distance between two successive detectedpoints.
  float epsilon_;

  // Points of the same date_ than the map will not be considered.
  date_t date_;

  float meanAngle_;
  unsigned short nbAngles_;
  float meanAperture_;
  unsigned short nbDist_;
  float maxDist_;
  bool littleAngle_;
};

# include "carto.hxx"

#endif /* !CARTO_HH_ */
