/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef PGM_HH_
# define PGM_HH_

# include <fstream>
# include <stdint.h>

class Pgm
{
public:
  Pgm (const char* filename, uint16_t w, uint16_t h);

  bool is_open ();
  void close ();

  Pgm& operator<< (uint8_t d);

private:
  std::ofstream file_;
};

union converter
{
  uint8_t value;
  char bin;
};

# include "pgm.hxx"

#endif /* !PGM_HH_ */
