/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#include "way.hh"

#include "cell.hh"

Way::Way (Carto* carto)
{
  inCellx_ = ABERRANT_VALUE; // necessary before initializing every way
  carto_ = carto;
}

Way::~Way ()
{
}

void Way::set (float beginx, float beginy, float vecx, float vecy)
{
  scaled_x_ = beginx / carto_->getStep ();
  scaled_y_ = beginy / carto_->getStep ();
  // moving cell in the grid
  cellx_ = floor (scaled_x_);
  celly_ = floor (scaled_y_);
  // position in the cell
  inCellx_ = scaled_x_ - cellx_;
  inCelly_ = scaled_y_ - celly_;
  // way to go
  vecx_ = vecx / carto_->getStep ();
  vecy_ = vecy / carto_->getStep ();
}

Cell* Way::hop ()
{
  // new way not initialized (never initialized, or known end already reached)
  if (inCellx_ > ABERRANT_VALUE / 2)
    return nullptr;

  // current visited cell
  Cell* oncell = carto_->cell (cellx_, celly_);

  // (u,v) will describe how to move to the next cell which is crossed by View
  char u = 0;
  char v = 0;

  // relative move on the vector View
  float eps = 0;

  scaled_x_ = cellx_ + inCellx_;
  scaled_y_ = celly_ + inCelly_;

  float lambda = (fabs (vecx_) > FLT_EPSILON ? ((vecx_ > 0) - inCellx_) / vecx_
		  : 42);
  float mu = (fabs (vecy_) > FLT_EPSILON ? ((vecy_ > 0) - inCelly_) / vecy_
	      : 42);
  inCellx_ = inCellx_ + mu * vecx_;
  inCelly_ = inCelly_ + lambda * vecy_;
  if (lambda - mu <= FLT_EPSILON && lambda <= 1.f + FLT_EPSILON)
  {
    u = (vecx_ > 0 ? 1 : -1);
    eps = lambda;
    inCellx_ = (vecx_ < 0);
  }
  else if (mu - lambda <= FLT_EPSILON && mu <= 1.f + FLT_EPSILON)
  {
    v = (vecy_ > 0 ? 1 : -1);
    eps = mu;
    inCelly_ = (vecy_ < 0);
  }

  if (u == 0 && v == 0)
  {
    inCellx_ = ABERRANT_VALUE;
    scaled_x_ += vecx_ / 2;
    scaled_y_ += vecy_ / 2;
  }
  else
  {
    scaled_x_ += eps * vecx_ / 2;
    scaled_y_ += eps * vecy_ / 2;
    if (eps >= 1.f - FLT_EPSILON)
    {
      vecx_ = 0.f;
      vecy_ = 0.f;
    }
    else
    {
      vecx_ -= eps * vecx_;
      vecy_ -= eps * vecy_;
    }
    cellx_ += u;
    celly_ += v;
  }

  return oncell;
}
