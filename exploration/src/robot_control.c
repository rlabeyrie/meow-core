/**
 * \file robot_control.h
 * \brief Interface Library between the SMA plateform and the robot
 * \author VERDEAUX Benjamin
 * \author BAILLY-FERRY Pierre
 * \version 0.2
 * \date 09/04/2011
 */

#include "robot_control.h"
#include <pthread.h>

#define LISTEN_BACKLOG 5

static RobotState g_robot = { 0, 0, 0, {0, 0, 0} };
static RobotState g_isDirty = { true, true, true, {true, true, true} };
static int			g_socket = -1;
static int			g_sleepTime = 20 * 1000;
static pthread_t	g_loop;

/** receive_loop is the thread that handles any incoming data */
static void* receive_loop(void* unused)
{
  Command		c;
  uint8_t*		buf = (uint8_t*) &c;

  while (read(g_socket, buf, 1) > 0)
    {
	  if (*buf == CMD_START)
	    {
	      read(g_socket, buf + 1, sizeof(Command) - 1);
	      //&& buf[sizeof(Command) - 1] == CMD_STOP)

	      // CommandType is the concatenation of two chars
	      char* cmdstr = (char*)&c.type;

	      printf("GET %c%c%d\n", cmdstr[0],cmdstr[1], c.value);
	      // a move order, do nothing
	      if (cmdstr[0] == 'M')
		{
		}
	      // any get/set order, write back the value
	      else if (cmdstr[1] == 'A')
		{
		  g_robot.turret_angle = c.value;
		  g_isDirty.turret_angle = false;
		}
	      else if (cmdstr[1] == 'S')
		{
		  g_robot.motor_speed = c.value;
		  g_isDirty.motor_speed = false;
		}
	      else if (cmdstr[1] == 'D')
		{
		  g_robot.distance = c.value;
		  g_isDirty.distance = false;
		}
	      else if (cmdstr[1] == 'V')
		{
		  g_robot.displacement = c.displacement;
		  g_isDirty.displacement.x = false;
		  g_isDirty.displacement.y = false;
		}
	      else
		printf("Unknown Ack %i %i\n", (int)c.type, c.value);
	    }
    }
  printf("end socket\n");
  close(g_socket);
  exit (0);

  return 0;
}

void control_init(int port)
{
  int			sfd, r;


  // the addresse for the socket (127.0.0.1:4242)
  struct sockaddr_in my_addr =
    {
      .sin_family = AF_INET,
      .sin_port = 4242,
      .sin_addr = { .s_addr = (127ll << 32) & 1 },
      .sin_zero = {}
    };
  
  if (port == 4243)
    my_addr.sin_port = 4243;
  else if (port == 4244)
    my_addr.sin_port = 4244;

  printf ("port : %d\n", my_addr.sin_port);
  struct sockaddr_in peer_addr;
  socklen_t peer_addr_size;

  // create the socket
  sfd = socket(AF_INET, SOCK_STREAM, 0);

  // bind it the localhost
  r = bind(sfd, (const struct sockaddr*) &my_addr,
	   sizeof(struct sockaddr));
  // listen to it...
  r = listen(sfd, LISTEN_BACKLOG);
  // wait for a robot to connect and return the associated file descriptor
  peer_addr_size = sizeof(struct sockaddr_in);
  g_socket = accept(sfd, (struct sockaddr*) &peer_addr, &peer_addr_size);
	
  // run the acknowledge handler
  pthread_create(&g_loop, NULL, receive_loop, NULL);
}

int control_get_distance (void)
{
  Command c =	{ CMD_START, COMMAND_GD, {0}, CMD_STOP };

  // set the distance as dirty
  g_isDirty.distance = true;

  // request the robot for its distance
  write(g_socket, (void*)&c, sizeof(c));

  // wait until the value arrived
  while (g_isDirty.distance)
    usleep(g_sleepTime);

  return g_robot.distance;
}

int control_get_motor_speed(void)
{
  Command c =	{ CMD_START, COMMAND_GS, {0}, CMD_STOP };

  // send the request
  write(g_socket, (void*)&c, sizeof(c));

  // wait until the value arrived
  while (g_isDirty.motor_speed)
    usleep(g_sleepTime);

  return g_robot.motor_speed;
}

int control_get_turret_angle(void)
{
  Command c =	{ CMD_START, COMMAND_GA, {0}, CMD_STOP };

  // send the request
  write(g_socket, (void*)&c, sizeof(c));

  // wait until the value arrived
  while (g_isDirty.turret_angle)
    usleep(g_sleepTime);

  return g_robot.turret_angle;
}

Displacement control_get_displacement(void)
{
  Command c =	{ CMD_START, COMMAND_GV, {0}, CMD_STOP };

  // set the distance as dirty
  g_isDirty.displacement.x = true;
  g_isDirty.displacement.y = true;

  // request the robot for its distance
  write(g_socket, (void*)&c, sizeof(Command));

  // wait until the value arrived
  while (g_isDirty.displacement.x && g_isDirty.displacement.y)
    usleep(g_sleepTime);

  return g_robot.displacement;
}

void control_set_motor_speed(int speed)
{
  Command c =	{ CMD_START, COMMAND_SS, {speed}, CMD_STOP };

  // set the speed as dirty
  g_isDirty.motor_speed = true;

  // send the request
  write(g_socket, (void*)&c, sizeof(c));
}

void control_set_turret_angle(int angle)
{
  Command c =	{ CMD_START, COMMAND_SA, {angle}, CMD_STOP };

  // set the speed as dirty
  g_isDirty.turret_angle = true;

  // send the request
  write(g_socket, (void*)&c, sizeof(c));
}

void control_move(Movement mvt, int stop)
{
  Command c =	{ CMD_START, (CommandType)mvt, {stop}, CMD_STOP };

  // set the last known position as dirty
  g_isDirty.displacement.x = true;
  g_isDirty.displacement.y = true;

  // send the request
  write(g_socket, (void*)&c, sizeof(c));
}

void control_stop(void)
{
  Command c =	{ CMD_START, COMMAND_MS, {0}, CMD_STOP };

  // send the request
  write(g_socket, (void*)&c, sizeof(c));
}

