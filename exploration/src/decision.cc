/**
 * \file decision.hh
 * \brief Take decision for the robot.
 * \author BAILLY-FERRY Pierre
 * \version 0.1
 * \date 09/01/2012
 */

#include "decision.hh"

Decision::Decision (Carto* carto)
{
  carto_ = carto;
  nextMovementType_ = STRAIGHT;
  for (int i = 0; i <= 18; ++i)
    IRtab_[i] = 0.0;
}

Decision::~Decision ()
{
}

void Decision::step()
{
  IRtab_[9] = control_get_distance();

  switch (nextMovementType_) {
  case STRAIGHT:
    straight();
    break;
  case TURN_LEFT:
    turn_left();
    break;
  case TURN_RIGHT:
    turn_right();
    break;
  case DODGE_LEFT:
    dodge_left();
    break;
  case DODGE_RIGHT:
    dodge_right();
    break;
  default:
    break;
  }
  next_step();
}

void Decision::next_step()
{
  control_set_turret_angle(90);
  carto_->turn(-PI / 2.0);
  IRtab_[0] = control_get_distance();
  if (IRtab_[0] < -0.5)
    carto_->nodetect(IRtab_[0]);
  else
    carto_->detect(IRtab_[0]);

  for (int i = 1; i < 19; i++)
    {
      control_set_turret_angle(-11);
      carto_->turn(PI / 18.0);
      IRtab_[i] = control_get_distance();
      if (IRtab_[i] < 0.5)
	carto_->nodetect(100);
      else
	carto_->detect(IRtab_[i]);
    }

  control_set_turret_angle(92);
  carto_->turn(-PI / 2.0);

  std::cout << IRtab_[9] << std::endl;
  if ((IRtab_[9] < -0.5 || IRtab_[9] > 30)
      && (IRtab_[7] < -0.5 || IRtab_[7] > 30)
      && (IRtab_[11] < -0.5 || IRtab_[11] > 30))
      nextMovementType_ = STRAIGHT;
  else
    {
      bool dodgeRight = (((IRtab_[8] < -0.5 || IRtab_[8] > 40)
			  || (IRtab_[7] < -0.5 || IRtab_[7] > 45)
			  || (IRtab_[6] < -0.5 || IRtab_[6] > 50))
			 && (IRtab_[0] < -0.5 || IRtab_[0] > 50));
      bool dodgeLeft = (((IRtab_[10] < -0.5 || IRtab_[10] > 40)
			 || (IRtab_[11] < -0.5 || IRtab_[11] > 45)
			 || (IRtab_[12] < -0.5 || IRtab_[12] > 50))
			&& (IRtab_[18] < -0.5 || IRtab_[18] > 50));
      if (dodgeRight && dodgeLeft)
	{
	  std::cout << "random" << std::endl;
	  if (rand() % 2 == 0)
	    nextMovementType_ = DODGE_RIGHT;
	  else
	    nextMovementType_ = DODGE_LEFT;
	}
      if (dodgeRight)
	nextMovementType_ = DODGE_RIGHT;
      else if (dodgeLeft)
	nextMovementType_ = DODGE_LEFT;
      /*
      else if ((IRtab_[18] > 0 && IRtab_[18] < 30)
	  || right_interesting())
	nextMovementType_ = TURN_RIGHT;
      else if ((IRtab_[0] > 0 && IRtab_[0] < 30)
	       || left_interesting())
	nextMovementType_ = TURN_LEFT;
      */
      else
	if (rand() %2 == 0)
	  nextMovementType_ = TURN_RIGHT;
	else
	  nextMovementType_ = TURN_LEFT;
    }
}

bool Decision::right_interesting()
{
  float angle = carto_->angle();

  int x = carto_->posX();
  int y = carto_->posY();

  if (angle > PI / 4.0)
    while (angle > 9.0 * PI / 4.0)
      angle -= 2 * PI;
  else
    while (angle < PI / 4.0)
      angle += 2 * PI;

  if (angle < 3.0 * PI / 4.0)
    {
      // looking left
      Cell* c = carto_->cell(x + 5, y, false);
      return (!c or c->color() != 100);
    }
  else if (angle < 5.0 * PI / 4.0)
    {
      // looking down
      Cell* c = carto_->cell(x, y + 5, false);
      return (!c or c->color() != 100);

    }
  else if (angle < 7.0 * PI / 4.0)
    {
      // looking right
      Cell* c = carto_->cell(x - 5, y, false);
      return (!c or c->color() != 100);
    }
  else 
    {
      // looking up
      Cell* c = carto_->cell(x, y - 5, false);
      return (!c or c->color() != 100);
    }
  
}


bool Decision::left_interesting()
{
  float angle = carto_->angle();

  int x = carto_->posX();
  int y = carto_->posY();

  if (angle > PI / 4.0)
    while (angle > 9.0 * PI / 4.0)
      angle -= 2 * PI;
  else
    while (angle < PI / 4.0)
      angle += 2 * PI;


  if (angle < 3.0 * PI / 4.0)
    {
      // looking left
      Cell* c = carto_->cell(x - 5, y, false);
      return (!c or c->color() != 100);
    }
  else if (angle < 5.0 * PI / 4.0)
    {
      // looking down
      Cell* c = carto_->cell(x, y - 5, false);
      return (!c or c->color() != 100);

    }
  else if (angle < 7.0 * PI / 4.0)
    {
      // looking right
      Cell* c = carto_->cell(x + 5, y, false);
      return (!c or c->color() != 100);
    }
  else 
    {
      // looking up
      Cell* c = carto_->cell(x, y + 5, false);
      return (!c or c->color() != 100);
    }  
}

void Decision::straight()
{
  if (IRtab_[9] < 0.5)
    {
      carto_->nodetect(100);
      control_move(MOVE_FORWARD, 100);
      carto_->move (100);
    }
  else
    {
      carto_->detect(IRtab_[9]);
      control_move(MOVE_FORWARD, IRtab_[9] - 20);
      carto_->move (IRtab_[9] - 20.0);
    }
}

void Decision::turn_left()
{

  control_move(MOVE_LEFT, 90);
  carto_->turn (PI / 2.0);
}

void Decision::turn_right()
{
  control_move(MOVE_RIGHT, 90);
  carto_->turn (-PI / 2.0);
}


void Decision::dodge_left()
{
  control_move(MOVE_LEFT, 89);
  carto_->turn (PI / 2.0);
 
  IRtab_[9] = control_get_distance();

  // useless value to enter in the loop
  IRtab_[0] = 10;

  while ((IRtab_[9] < 0.5 || IRtab_[9] > 30)
	 && (IRtab_[0] > 0.5 && IRtab_[0] < 30))
    {
      control_move(MOVE_FORWARD, 10);
      carto_->move (10);
      control_set_turret_angle(90);
      carto_->turn(- PI / 2.0);
      IRtab_[0] = control_get_distance();
      if (IRtab_[0] < -0.5)
	carto_->nodetect(IRtab_[0]);
      else
	carto_->detect(IRtab_[0]);

      control_set_turret_angle(-91);
      carto_->turn(PI / 2.0);
      IRtab_[9] = control_get_distance();
      if (IRtab_[9] < -0.5)
	carto_->nodetect(IRtab_[9]);
      else
	carto_->detect(IRtab_[9]);
    }


  if (IRtab_[9] < 0.5 || IRtab_[9] > 30)
    {
      control_move(MOVE_FORWARD, 10);
      carto_->move (10);
    }

  control_move(MOVE_RIGHT, 90);
  carto_->turn (-PI / 2.0);
}


void Decision::dodge_right()
{
  control_move(MOVE_RIGHT, 89);
  carto_->turn (-PI / 2.0);
 
  IRtab_[9] = control_get_distance();

  // useless value to enter in the loop
  IRtab_[18] = 10;

  while ((IRtab_[9] < 0.5 || IRtab_[9] > 30)
	 && (IRtab_[18] > 0.5 && IRtab_[18] < 30))
    {
      control_move(MOVE_FORWARD, 10);
      carto_->move (10);
      control_set_turret_angle(-91);
      carto_->turn(PI / 2.0);
      IRtab_[18] = control_get_distance();
      if (IRtab_[18] < -0.5)
	carto_->nodetect(IRtab_[18]);
      else
	carto_->detect(IRtab_[18]);

      control_set_turret_angle(90);
      carto_->turn(- PI / 2.0);
      IRtab_[9] = control_get_distance();
      if (IRtab_[9] < -0.5)
	carto_->nodetect(IRtab_[9]);
      else
	carto_->detect(IRtab_[9]);
    }

  if (IRtab_[9] < 0.5 || IRtab_[9] > 30)
    {
      control_move(MOVE_FORWARD, 10);
      carto_->move (10);
    }

  control_move(MOVE_LEFT, 92);
  carto_->turn (PI / 2.0);
}


