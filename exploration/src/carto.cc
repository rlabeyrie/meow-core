/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#include "carto.hh"

#include "way.hh"
#include "pgm.hh"

// proportions of a trench: distance / aperture
#define TRENCH_RATIO 10.f

Carto::Carto (float decentStep)
  : previouslySeen (nullptr),
    nextToSee_ (nullptr),
    posx_ (0.f),
    posy_ (0.f),
    angle_ (0.f),
    moved_ (true),
    isWaitingPoint_ (false),
    lastWritten_ ({-1.f, nullptr, nullptr}),
    lastSeen_ ({-1.f, nullptr, nullptr}),
    step_ (decentStep),
    epsilon_ (0.f),
    date_ (0),
    meanAngle_ (0.f),
    nbAngles_ (0),
    meanAperture_ (0.f),
    nbDist_ (0),
    maxDist_ (0.f),
    littleAngle_ (false)
{
  assert (step_ > 0.1f); // find a more relevant assert (measures' precision?)

  way_ = new Way (this);
  pointWay = new Way (this);

  Point::initializeCarto (step_ * 0.5f, this);
}

Carto::~Carto ()
{
  delete way_;
  delete pointWay;

  for (auto x : xpositive_map_)
    for (auto y : x)
      delete y;
  for (auto x : xnegative_map_)
    for (auto y : x)
      delete y;
}

void Carto::detect (float distance)
{
  if (distance <= FLT_EPSILON)
    return;

  // CALLED FIRST: erase the empty area and calculate some things
  nodetect (distance, true);


  if (lastSeen_.lambda > 0.f)
  {
    if (lastSeen_.prev)
      lastWritten_ = lastSeen_;
    else
    {
      float x = posx_ + viewx_;
      float y = posy_ + viewy_;

      float maxd = epsilon_ * epsilon_;
      if (lastWritten_.prev)
      {
	float d1 = pow (lastWritten_.prev->x () - x, 2)
	  + pow (lastWritten_.prev->y () - y, 2);
	float d2 = pow (lastWritten_.next->x () - x, 2)
	  + pow (lastWritten_.next->y () - y, 2);
	if (d1 < maxd || d2 < maxd)
	{
	  bool fromPrev = (d1 < d2);
	  Point* from = (fromPrev ? lastWritten_.prev : lastWritten_.next);
	  from = from->addPoint (x, y, fromPrev, this, nullptr, false);
	  if (fromPrev)
	  {
	    lastWritten_.next = lastWritten_.prev;
	    lastWritten_.prev = from;
	  }
	  else
	  {
	    lastWritten_.prev = lastWritten_.next;
	    lastWritten_.next = from;
	  }
	}
	else
	  lastWritten_.prev = nullptr;
      }
      if (!lastWritten_.prev)
      {
	if (isWaitingPoint_
	    && pow (oldx_ - x, 2) + pow (oldy_ - y, 2) < maxd)
	{
	  Point::addCouple (x, y, oldx_, oldy_, this,
			    lastWritten_.prev, lastWritten_.next);
	  isWaitingPoint_ = false;
	}
	else
	{
	  oldx_ = x;
	  oldy_ = y;
	  isWaitingPoint_ = true;
	}
      }
    }
  }
}


void Carto::nodetect (float distance, bool objectDetected)
{
  if (distance <= FLT_EPSILON)
    return;

  meanAperture_ = (meanAperture_ * nbDist_ + distance / TRENCH_RATIO)
    / (nbDist_ + 1);
  if (nbDist_ < 100)
    ++nbDist_;

  viewx_ = distance * cos (angle_);
  viewy_ = distance * sin (angle_);

  if (!moved_ && distance <= distance_ + FLT_EPSILON)
  {
    lastSeen_.lambda = -1.f;
    return;
  }
  moved_ = false;

  if (distance > maxDist_ * 0.9f)
  {
    if (distance > maxDist_)
      maxDist_ = distance;
    epsilon_ = maxDist_ * sin (meanAngle_);
  }
  distance_ = distance;

  ++date_;

  assert (borders_.empty ());
  assert (invisited_.empty ());

  lastSeen_.lambda = (objectDetected ? 1.f + TOLERANCE : 1.f - TOLERANCE);
  lastSeen_.prev = nullptr;
  way_->set (posx_, posy_, viewx_ * lastSeen_.lambda,
	     viewy_ * lastSeen_.lambda);

  Cell* oncell = nullptr;
  while ((oncell = way_->hop ()))
    if (!oncell->isEmpty ())
      oncell->trench (this, lastSeen_);
  if (objectDetected && !lastSeen_.prev)
    lastSeen_.lambda = 1.f;

  // open and reconnect the borders of the unvisited world which are crossed
  while (!borders_.empty () && borders_.top ().lambda < lastSeen_.lambda)
  {
    float lambda = borders_.top ().lambda;
    Point* prev = borders_.top ().prev;
    Point* next = borders_.top ().next;
    borders_.pop ();

    prev->nextOnLine = nullptr;
    next->prevOnLine = nullptr;
    Point* openPrev1 = open (prev, false, lambda);
    Point* openNext1 = open (next, true, lambda);


    if (!borders_.empty () && borders_.top ().lambda < lastSeen_.lambda)
    {
      lambda = borders_.top ().lambda;
      prev = borders_.top ().prev;
      next = borders_.top ().next;
      borders_.pop ();

      prev->nextOnLine = nullptr;
      next->prevOnLine = nullptr;
      Point* openPrev2 = open (prev, false, lambda);
      Point* openNext2 = open (next, true, lambda);

      openPrev1->link (openNext2, this);
      openPrev2->link (openNext1, this);
    }
    else
      openPrev1->addPoint (posx_ + lastSeen_.lambda * viewx_,
			   posy_ + lastSeen_.lambda * viewy_,
			   false, this, openNext1);
  }
  while (!borders_.empty ())
    borders_.pop ();

  // open the obstacle-segments
  while (!invisited_.empty ())
  {
    float lambda = invisited_.top ().lambda;
    Point* prev = invisited_.top ().prev;
    Point* next = invisited_.top ().next;
    invisited_.pop ();

    prev->nextOnLine = nullptr;
    next->prevOnLine = nullptr;
    prev->addPoint (posx_ + lambda * viewx_,
		    posy_ + lambda * viewy_, false, this);
    next->addPoint (posx_ + lambda * viewx_,
		    posy_ + lambda * viewy_, true, this);
  }

  emptyTrash ();
}

Point* Carto::open (Point* from, bool toNext, float lambda)
{
  float d = 0.f;
  float lastD = 0.f;
  bool isDMax;
  Point* lastPt = nullptr;
  float dx;
  float dy;
  while (from && (isDMax
		  = ((d = (dx = from->x () - posx_) * viewy_
		      - (dy = from->y () - posy_) * viewx_)
		     * lastD > lastD * lastD - FLT_EPSILON))
	 && fabs (d) < meanAperture_
	 && dx * viewx_ + dy * viewy_ > 0.f)
  {
    if (lastPt)
      lastPt->remove ();
    lastPt = from;
    lastD = d;
    from = (toNext ? from->nextOnLine : from->prevOnLine);
  }

  Point* ret = lastPt;
  if (from && isDMax)
  {
    d = fabs (d);
    if (d < meanAperture_)
    {
      ret = from;
      if (lastPt)
	lastPt->remove ();
    }
    else
    {
      float lastPtx;
      float lastPty;
      if (lastPt)
      {
	lastPtx = lastPt->x ();
	lastPty = lastPt->y ();
	lastD = fabs (lastD);
	lastPt->remove ();
      }
      else
      {
	lastPtx = posx_ + lambda * viewx_;
	lastPty = posy_ + lambda * viewy_;
      }
      float r = (d - meanAperture_) / (d - lastD);
      float ptx = from->x () + (lastPtx - from->x ()) * r;
      float pty = from->y () + (lastPty - from->y ()) * r;
      ret = from->addPoint (ptx, pty, toNext, this, nullptr, false);
    }
  }

  return ret;
}

Cell* Carto::cell (int cellx, int celly, bool create)
{
  std::deque< std::deque<Cell*> >* grid = &xpositive_map_;
  std::deque<int>* origins = &xpositive_origins_;
  if (cellx < 0)
  {
    grid = &xnegative_map_;
    origins = &xnegative_origins_;
  }
  unsigned orig = abs (cellx) - (cellx < 0);
  unsigned x = orig * 2;
  if ((*origins).size () > orig && (*grid)[x].size () > 0
      && celly < (*origins)[orig])
    ++x;
  if ((*grid).size () <= x)
  {
    (*grid).resize (x + 1);
    (*origins).resize (orig + 1);
  }
  std::deque<Cell*>& vect = (*grid)[x];

  if ((*grid)[orig * 2].size () == 0)
    (*origins)[orig] = celly;
  unsigned y = abs (celly - (*origins)[orig]) - (celly < (*origins)[orig]);

  if (vect.size () <= y)
    vect.resize (y + 1, 0);
  if (!vect[y] && create)
    vect[y] = new Cell ();
  return vect[y];
}

void Carto::display (const char* filename)
{
  int size = 50;
  Pgm file (filename, 2 * size, 2 * size);
  if (file.is_open ())
  {
    for (int x = size; x > -size; --x)
      for (int y = size; y > -size; --y)
      {
	Cell* c = cell (x, y, false);
	if (c)
	  file << c->color ();
	else
	  file << UNVISITED_COLOR;
      }
    file.close ();
  }
  else
    std::cerr << "Fail to open file " << filename << std::endl;
}

void Carto::display_magnify (unsigned* img, int display_size,
			     int offset_x, int offset_y, int magnify)
{
  ++date_;
  assert (magnify > 0 && display_size > 10);
  int field_size = (display_size - 1) / magnify + 1;
  for (int i = 0; i < display_size; ++i)
    for (int j = 0; j < display_size; ++j)
      *(img + display_size * i + j) = VOID_COLOR_RGB;
  for (int x = 0; x < field_size; ++x)
    for (int y = 0; y < field_size; ++y)
    {
      int inx = x * magnify;
      int iny = y * magnify;
      Cell* c = cell (field_size / 2 - x + offset_y,
		      field_size / 2 - y + offset_x, false);
      if (c)
	c->colorize (img, display_size, magnify, inx, iny, step_, this);
      else
	for (int i = inx; i < inx + magnify && i < display_size; ++i)
	  for (int j = iny; j < iny + magnify && j < display_size; ++j)
	    *(img + display_size * i + j) = UNVISITED_COLOR_RGB;
    }
  float x1 = (field_size / 2 + offset_x - floor (posx_ / step_)) * magnify;
  float y1 = (field_size / 2 + offset_y - floor (posy_ / step_)) * magnify;
  float x2 = (field_size / 2 + offset_x - floor ((posx_ + viewx_) / step_))
    * magnify;
  float y2 = (field_size / 2 + offset_y - floor ((posy_ + viewy_) / step_))
    * magnify;
  drawSegments (img, display_size, x1, y1, x2, y2, VIEW_COLOR_RGB);
}

void Carto::display_reduce (unsigned* img, int display_size,
			    int offset_x, int offset_y, int reduce)
{
  assert (reduce > 0);
  for (int x = 0; x < display_size; ++x)
    for (int y = 0; y < display_size; ++y)
    {
      int nx = (display_size / 2 - x + offset_y) * reduce;
      int ny = (display_size / 2 - y + offset_x) * reduce;
      unsigned final_col = UNVISITED_COLOR_RGB;
      for (int i = 0; i < reduce && final_col != UNKNOWN_COLOR_RGB; ++i)
  	for (int j = 0; j < reduce && final_col != UNKNOWN_COLOR_RGB; ++j)
  	{
  	  Cell* c = cell (nx + i, ny + j, false);
  	  if (c)
  	  {
  	    unsigned int col = c->color_rgb ();
	    if (final_col != KNOWN_COLOR_RGB || col == UNKNOWN_COLOR_RGB)
	      final_col = col;
  	  }
  	}
      *(img + display_size * x + y) = final_col;
    }
}
