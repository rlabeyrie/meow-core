/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef CELL_HXX_
# define CELL_HXX_

# define UNKNOWN_COLOR 100
# define KNOWN_COLOR 0
# define VOID_COLOR 255
# define UNVISITED_COLOR 200

# define UNKNOWN_COLOR_RGB 16711680 // red
# define KNOWN_COLOR_RGB 0 // black
# define VOID_COLOR_RGB 16777215 // white
# define UNVISITED_COLOR_RGB 9868950 // light gray
# define SEGMENT_COLOR_RGB 100 // light blue

# define VIEW_COLOR_RGB 51200 // green

inline
bool Cell::isEmpty ()
{
  return !first_;
}

inline
unsigned Cell::color ()
{
  if (first_)
  {
    if (unknownPoints_)
      return UNKNOWN_COLOR;
    else
      return KNOWN_COLOR;
  }
  else
    return VOID_COLOR;
}

inline
unsigned Cell::color_rgb ()
{
  if (first_)
  {
    if (unknownPoints_)
      return UNKNOWN_COLOR_RGB;
    else
      return KNOWN_COLOR_RGB;
  }
  else
    return VOID_COLOR_RGB;
}

inline
void Cell::removeFirst ()
{
  first_ = first_->nextInCell;
}

inline
void Cell::delUnknown ()
{
  --unknownPoints_;
}

#endif /* !CELL_HXX_ */
