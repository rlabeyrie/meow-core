/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef CARTO_HXX_
# define CARTO_HXX_

inline
void Carto::move (float distance)
{
  nodetect (distance / (1.f - 2 * TOLERANCE));
  posx_ += distance * cos (angle_);
  posy_ += distance * sin (angle_);
  moved_ = true;
  littleAngle_ = false;
}

inline
void Carto::turn (float angle)
{
  angle_ += angle; // angle in radians.
  moved_ = true;
  angle = fabs (angle);
  if (!nbAngles_)
  {
    nbAngles_ = 1;
    meanAngle_ = angle;
    epsilon_ = maxDist_ * sin (angle);
    littleAngle_ = true;
  }
  else if (angle < meanAngle_ * 2)
  {
    meanAngle_ = (meanAngle_ * nbAngles_ + angle) / (nbAngles_ + 1);
    if (nbAngles_ < 100)
      ++nbAngles_;
    littleAngle_ = true;
  }
  else
    littleAngle_ = false;
}

inline
date_t Carto::date ()
{
  return date_;
}

inline
Cell* Carto::findCell (Point* pt)
{
  return cell (floor (pt->x () / step_), floor (pt->y () / step_));
}

inline
void Carto::add (Point* pt)
{
  findCell (pt)->add (pt, this);
}

inline
void Carto::remove (Point* pt) // FIXME: useless
{
  if (pt->prevOnLine)
    pt->prevOnLine->nextOnLine = pt->nextOnLine;
  if (pt->nextOnLine)
    pt->nextOnLine->prevOnLine = pt->prevOnLine;

  pt->prevOnLine = nullptr;
  pt->nextOnLine = nullptr;
}

inline
void Carto::addBorderSegment (float lambda, Point* prev, Point* next)
{
  borders_.push ({lambda, prev, next});
}

inline
void Carto::addInVisitedSegment (float lambda, Point* prev, Point* next)
{
  invisited_.push ({lambda, prev, next});
}

inline
float Carto::posX ()
{
  return posx_;
}

inline
float Carto::posY ()
{
  return posy_;
}

inline
float Carto::angle ()
{
  return angle_;
}

inline
float Carto::viewX ()
{
  return viewx_;
}

inline
float Carto::viewY ()
{
  return viewy_;
}

// inline
// float Carto::viewNorm ()
// {
//   return distance_;
// }

inline
float Carto::epsilon ()
{
  return epsilon_;
}

inline
float Carto::getStep ()
{
  return step_;
}

// inline
// bool Carto::purgeKept ()
// {
//   while (!(kept.empty ()
// 	   || kept.top ().prev->nextOnLine || kept.top ().next->prevOnLine))
//     kept.pop ();
//   return !kept.empty ();
// }

// inline
// float Carto::getAperture ()
// {
//   return meanAperture_;
// }

inline
void Carto::initCellCover (Cell* cell, Point* first)
{
  trashCells_.push (cell);
  trashPoints_.push (nullptr);
  previouslySeen = nullptr;
  nextToSee_ = first;
}

inline
bool Carto::isCurrent (Cell* cell)
{
  return (trashCells_.size () && cell == trashCells_.top ());
}

inline
void Carto::seeNext ()
{
  previouslySeen = nextToSee_;
  nextToSee_ = nextToSee_->nextInCell;
  nextToSee_ = nextToSee ();
}

inline
Point* Carto::nextToSee ()
{
  while (nextToSee_ && !(nextToSee_->prevOnLine || nextToSee_->nextOnLine))
  {
    Point* next = nextToSee_->nextInCell;
    if (previouslySeen)
      previouslySeen->nextInCell = next;
    else
      trashCells_.top ()->removeFirst ();
    nextToSee_->nextInCell = trashPoints_.top ();
    trashPoints_.top () = nextToSee_;
    nextToSee_ = next;
  }
  return nextToSee_;
}

inline
void Carto::emptyTrash ()
{
  assert (trashCells_.size () == trashPoints_.size ());
  while (trashCells_.size ())
  {
    Cell* cell = trashCells_.top ();
    Point* trash = trashPoints_.top ();
    while (trash)
    {
      if (!trash->isObstacle ())
	cell->delUnknown ();
      Point* remember = trash;
      trash = trash->nextInCell;
      if (remember->prevOnLine || remember->nextOnLine)
	cell->add (remember, this);
      else
	delete remember;
    }
    trashCells_.pop ();
    trashPoints_.pop ();
  }
}

inline
void Carto::drawSegments (unsigned* img, int display_size, int x1, int y1,
			 int x2, int y2, unsigned col)
{
  int vecx = x2 - x1;
  int vecy = y2 - y1;
  int vec = (abs (vecx) > abs (vecy) ? abs(vecx) : abs(vecy));
  for (int i = 1; i < vec; ++i)
  {
    int x = x1 + i * vecx / vec;
    int y = y1 + i * vecy / vec;
    if (x >= 0 && y >= 0 && x < display_size && y < display_size)
    {
      int index = display_size * x + y;
      if (img[index] == VOID_COLOR_RGB)
	img[index] = col;
    }
  }
}

#endif /* !CARTO_HXX_ */
