/**
 * \file robot_control.h
 * \brief Interface Library between the SMA plateform and the robot
 * \author VERDEAUX Benjamin
 * \author BAILLY-FERRY Pierre
 * \version 0.2
 * \date 09/04/2011
 */

#ifndef ROBOT_CONTROL_H_
# define ROBOT_CONTROL_H_

# include "limits.h"
# define INFINITE INT_MAX;

# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <string.h>
# include <unistd.h>
# include <stdio.h>
# include <fcntl.h>
# include <stdlib.h>

# include "types.h"



/** \brief Initializes the server, and waits for the first robot.
 */
void control_init(int port);




/** \brief Requests the connected robot to give the value of its distance
 * sensor.
 * \return The value sent by the robot, in millimeters.
 * If the sensor cannot read the distance, a negative value is returned.
 *
 * \warning This function waits for a response from the client and could
 * take time.*/
int control_get_distance (void);

/** \brief Requests the connected robot to give the angle of its distance
 * sensor.
 * \return The value sent by the robot, in degree
 *
 * \warning This function waits for a response from the client and could
 * take time.*/
int control_get_turret_angle(void);

/** \brief Requests the connected robot to give the image seen by its camera.
 * \return A byte array containing the image data.
 *
 * \warning This function waits for a response from the client and could
 * take time.*/
char* control_get_image (void);

/** \brief Get the displacement of the robot since the last call of this
 * function (or of control_init).
 */
Displacement control_get_displacement(void);

/** \brief Gets the speed for every further moves of the given kind.
 */
int control_get_motor_speed (void);




/** \brief Sets the speed for every further moves of the given kind.
 * \param speed The desired speed
 */
void control_set_motor_speed (int speed);

/** \brief Sets the new angle of the turret.
 * \param angle The desired angle relative to the initial angle.*/
void control_set_turret_angle(int angle);




/** \brief Sends the connected robot a move directive. If the robot is moving,
 * it will finish its move, then act as directed until it receives a stop
 * request.
 * \param mvt The move kind (CONTROL_GO, CONTROL_TURN, CONTROL_LOOK).
 * \param stop (optional) If set, the robot will stop after the given value.
 * \return The latest travelled distance, in millimeters or degrees depending
 * on the move kind.*/
void control_move (Movement mvt, int stop);

/** \brief Requests the robot to stop its move.
 * \return The latest traveled distance, in millimeters or degrees depending
 * on the move kind.*/
void control_stop (void);


#endif /* !ROBOT_CONTROL_H_ */
