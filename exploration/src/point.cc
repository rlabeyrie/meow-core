/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#include "point.hh"

#include "carto.hh"
#include "way.hh"

#define COS2_LIMIT 0.5f // has to be > 0 and < 1
// if you want to consider three points A, B, C aligned and ordered,
// you must have cos^2 (BA, BC) > COS2_LIMIT (e.g. angle > 135° => C2L = 0.5f)
static_assert (COS2_LIMIT > 0.1f && COS2_LIMIT < 1.f, "Aberrant alignment!");

Point::Point (float x, float y, Point* prev, Point* next, Carto* carto,
	      bool addToCell)
  : prevOnLine (prev),
    nextOnLine (next),
    x_ (x),
    y_ (y),
    date_ (carto->date ()),
    isObstacle_ (false)
{
  if (prev)
  {
    if (prev->nextOnLine)
      prev->nextOnLine->prevOnLine = nullptr; // FIXME: useless?
    prev->nextOnLine = this;
    isVisited_ = prev->isVisited ();
    if (prev->isVisited () && prev->prevOnLine)
      prev->setObstacle (true);
    if (next)
    {
      if (next->prevOnLine)
	next->prevOnLine->nextOnLine = nullptr;
      next->prevOnLine = this;
      if (isVisited_)
	isObstacle_ = true;
      if (next->isVisited () && next->nextOnLine)
	next->setObstacle (true);
    }
  }
  else if (next)
  {
    if (next->prevOnLine)
      next->prevOnLine->nextOnLine = nullptr;
    next->prevOnLine = this;
    isVisited_ = next->isVisited ();
    if (next->isVisited () && next->nextOnLine)
      next->setObstacle (true);
  }
  else
    isVisited_ = true;

  if (addToCell)
    carto->add (this);
}

Point::Point (float x, float y, Carto* carto)
  : prevOnLine (nullptr),
    nextOnLine (nullptr),
    x_ (x),
    y_ (y),
    date_ (carto->date ()),
    isObstacle_ (false)
{
  isVisited_ = false;
  carto->add (this);
}

float Point::testNeighbor (Carto* carto, Point* neighbor)
{
  float ret = ABERRANT_VALUE;
  if (neighbor && !neighbor->up_to_date (carto->date ()))
  {
    float posx = carto->posX ();
    float posy = carto->posY ();
    float viewx = carto->viewX ();
    float viewy = carto->viewY ();
    float vecx = neighbor->x () - x_;
    float vecy = neighbor->y () - y_;
    float lambda = vecy * (x_ - posx) + vecx * (posy - y_);
    if (fabs (lambda) > FLT_MIN)
      lambda /= (vecy * viewx - vecx * viewy);
    if (lambda > 0.f && lambda < 1.f + TOLERANCE)
    {
      float mu;
      if (fabs (vecx) > fabs (vecy))
	mu = (posx - x_ + lambda * viewx) / vecx;
      else
	mu = (posy - y_ + lambda * viewy) / vecy;
      if (mu >= -FLT_EPSILON && mu <= 1.f + FLT_EPSILON)
	ret = lambda;
    }
  }
  date_ = carto->date ();
  return ret;
}

Point* Point::addPoint (float x, float y, bool madeFromItsNext,
			Carto* carto, Point* otherSide, bool removable)
{
  Point* prev = (madeFromItsNext ? otherSide : this);
  Point* next = (madeFromItsNext ? this : otherSide);

  Point* ret = nullptr;
  if (prev && next && ordAligned (prev, x, y, next->x (), next->y ()))
  {
    Point* from = prev;
    Point* to = next;
    prev->nextOnLine = next;
    next->prevOnLine = prev;
    if (prev->prevOnLine && ordAligned (prev->prevOnLine, prev, x, y, next))
      from = prev->prevOnLine;
    if (next->nextOnLine && ordAligned (next->nextOnLine, next, x, y, prev))
      to = next->nextOnLine;
    distribute (from, to, carto);
  }
  else
  {
    // remove eventually small line, otherwise create a point
    if (!removable || (!(prev && prev->isVisited () && !next
			 && eraseResidue (x, y, prev, false, carto))
		       && !(next && next->isVisited () && !prev
			    && eraseResidue (x, y, next, true, carto))))
    {
      ret = new Point (x, y, prev, next, carto);
      if (prev)
      {
	if (prev->prevOnLine
	    && ordAligned (prev->prevOnLine, prev->x (), prev->y (),
			   ret->x (), ret->y ()))
	  distribute (prev->prevOnLine, ret, carto);
	else
	  distribute (prev, ret, carto);
      }
      if (next)
      {
	if (next->nextOnLine
	    && ordAligned (next->nextOnLine, next->x (), next->y (),
			   ret->x (), ret->y ()))
	  distribute (ret, next->nextOnLine, carto);
	else
	  distribute (ret, next, carto);
      }
    }
  }
  return ret;
}

bool Point::ordAligned (Point* pt1, float x2, float y2, float x3, float y3)
{
  // law of cosines: c² = a² + b² - 2 * a * b * cos(C)
  // let num = a² + b² - c²:
  // is num < 0 and num² / (4 * a² * b²) = cos²(C) > COS2_LIMIT?
  float b2 = pow (pt1->x () - x2, 2) + pow (pt1->y () - y2, 2);
  float a2 = pow (x3 - x2, 2) + pow (y3 - y2, 2);
  float num = a2 + b2 - (pow (pt1->x () - x3, 2) + pow (pt1->y () - y3, 2));
  return (num < 0 && pow (num, 2) / (4.f * a2 * b2) > COS2_LIMIT);
}

void Point::distribute (Point* from, Point* to, Carto* carto)
{
  assert (from && to && carto);
  Cell* oncell = (from != to ? carto->findCell (from) : nullptr);
  while (from != to)
  {
    bool together;
    while ((together = (carto->findCell (from->nextOnLine) == oncell))
	   && from->nextOnLine != to)
      from->nextOnLine->remove ();
    Point* temp_to = from->nextOnLine;
    if (!together)
    {
      carto->pointWay->set (from->x (), from->y (),
			    temp_to->x () - from->x (),
			    temp_to->y () - from->y ());
      Cell* last = carto->pointWay->hop ();
      oncell = carto->pointWay->hop ();
      while (oncell)
      {
	float x = carto->pointWay->x ();
	float y = carto->pointWay->y ();
	last = oncell;
	if ((oncell = carto->pointWay->hop ()))
	{
	  from = new Point (x, y, from, temp_to, carto, false);
	  last->add (from, carto);
	}
      }
      oncell = last;
    }
    from = temp_to;
  }
}

bool Point::eraseResidue (float x, float y, Point* pt, bool toNext,
			  Carto* carto)
{
  assert (pt);

  float ymax = y;
  float ymin = ymax;
  float xmax = x;
  float xmin = xmax;
  float limit = carto->epsilon () / 2;
  Point* it = pt;
  do
  {
    if (it->x () < xmin)
      xmin = it->x ();
    else if (it->x () > xmax)
      xmax = it->x ();
    if (it->y () < ymin)
      ymin = it->y ();
    else if (it->y () > ymax)
      ymax = it->y ();
    it = (toNext ? it->nextOnLine : it->prevOnLine);
  }
  while (ymax - ymin < limit && xmax - xmin < limit && it);
  if (ymax - ymin < limit && xmax - xmin < limit)
  {
    while (pt)
    {
      it = pt;
      pt = (toNext ? pt->nextOnLine : pt->prevOnLine);
      it->remove ();
    }
    return true;
  }
  return false;
}

bool Point::eraseResidue (Carto* carto)
{
  float ymax = y_;
  float ymin = ymax;
  float xmax = x_;
  float xmin = xmax;
  float limit = carto->epsilon () / 2;
  Point* it = nextOnLine;
  do
  {
    if (it->x () < xmin)
      xmin = it->x ();
    else if (it->x () > xmax)
      xmax = it->x ();
    if (it->y () < ymin)
      ymin = it->y ();
    else if (it->y () > ymax)
      ymax = it->y ();
    it = it->nextOnLine;
  }
  while (ymax - ymin < limit && xmax - xmin < limit && it && it != this);
  if (ymax - ymin < limit && xmax - xmin < limit && it)
    do
    {
      Point* del = it;
      it = it->nextOnLine;
      del->remove ();
    }
    while (it);
  return !nextOnLine;
}
