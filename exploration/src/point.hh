/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef POINT_HH_
# define POINT_HH_

#include <cassert> // FIXME: not supposed to be kept
#include <iostream>
#include <cfloat>
#include <math.h> // for fabs ()

/// \def tolerance of drifts (0 < T << 1, 0 = no drift possible)
# define TOLERANCE 0.1f
static_assert (TOLERANCE > 0.0001f && TOLERANCE < 0.2f, "Wrong tolerance!");

# define ABERRANT_VALUE 42 // should be > 4
static_assert (ABERRANT_VALUE > 4, "Wrong value for 'ABERRANT_VALUE'!");

typedef unsigned short date_t;

class Carto; // forward dec
class Way; // forward dec

/// \brief Any point of the map, describing either
/// an obstacle or a border between known and unknown world.
/// \details The only constructor of Point is private because it has to be
/// called by an updating method, from another point of the map. Only
/// couples of points (simultaneously) can be created from scratch.
class Point
{
public:
  static void initializeCarto (float xy, Carto* carto);

  /// A couple of points can be created from scratch by this method,
  /// whereas an only point cannot. Call the private constructor.
  /// The two created points are set in point1 and point2
  static void addCouple (float x1, float y1, float x2, float y2,
			 Carto* carto, Point*& prev, Point*& next);

  /// Add a point from another, which can decide where the new point
  /// should be added: `prev' and `next' cannot be both nullptr.
  /// Create a point if it is not in an alignment, otherwise
  /// reorganize if necessary. It can even decide to not add this point
  /// and to remove some other ones if `removable' is true!
  /// \return The point if created.
  Point* addPoint (float x, float y, bool madeFromItsNext, Carto* carto,
		   Point* otherSide = nullptr, bool removable = true);

  /// Let `this' be removed later from the list of points of its cell.
  void remove ();

  /// \return lambda such as (posx + lambda * viewx, posy + lambda * viewy)
  /// is on the segment [this ; neighbor] (Pos and View given by Carto),
  /// and lambda < 1 + TOLERANCE; otherwise ABERRANT_LAMBDA.
  float testNeighbor (Carto* carto, Point* neighbor);

  float x ();
  float y ();

  /// All points are joined in a strict order along lines.
  Point* prevOnLine;
  /// All points are joined in a strict order along lines.
  Point* nextOnLine;

  /// Points make a home-made double linked list in each cell.
  Point* nextInCell;

  void update (date_t date);

  /// \return true if the point has already been updated (by Point::update ()).
  bool up_to_date (date_t date);

  /// \return true if the point describes an obstacle.
  bool isObstacle ();
  /// \return true if the point is in the previously visited world.
  bool isVisited ();

  /// Set this point as an obstacle if param is true, as a border otherwise.
  void setObstacle (bool isObstacle);

  /// Link the two points `this' and `tonext', eventually creating points.
  void link (Point* tonext, Carto* carto);

private:
  Point (float x, float y, Point* prev, Point* next, Carto* carto,
	 bool addToCell = true);

  Point (float x, float y, Carto* carto);

  // Check if the three points pt1, (x2,y2), (x3,y3) are aligned and ordered.
  bool ordAligned (Point* pt1, float x2, float y2, float x3, float y3);

  // Check if the four points pt1, pt2, (x3,y3), pt4 are aligned and ordered.
  bool ordAligned (Point* pt1, Point* pt2, float x3, float y3, Point* pt4);

  // Eventually add and erase points to be sure there is exactly one point
  // in each Cell crossed by the segment [from, to], on this segment.
  void distribute (Point* from, Point* to, Carto* carto);

  // Evaluate the potential branch starting from (x, y) then `pt' and step
  // by step by the "nextOnLine"s if `toNext' is true, by "prevOnLine"s
  // otherwise. Erase the entire branch if it could take place in a square
  // limit x limit (with limit defined by carto).
  // Return true in this case, false otherwise.
  bool eraseResidue (float x, float y, Point* pt, bool toNext, Carto* carto);

  bool eraseResidue (Carto* carto);

  float x_;
  float y_;

  // Every point has a date. If its date is smaller than the map's one,
  // it will be updated.
  date_t date_;

  // true if the point describes an obstacle; false if it is a border
  // (even in the visited world), telling that something is unknown.
  bool isObstacle_;
  // If the point is on a border of the never-visited world, `isVisited_' is
  // false. true = all around it has already been visited at least once.
  bool isVisited_;
};

# include "point.hxx"

#endif /* !POINT_HH_ */
