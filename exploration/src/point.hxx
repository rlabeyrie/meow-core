/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef POINT_HXX_
# define POINT_HXX_

inline
void Point::addCouple (float x1, float y1, float x2, float y2,
		       Carto* carto, Point*& prev, Point*& next)
{
  prev = new Point (x1, y1, nullptr, nullptr, carto);
  next = prev->addPoint (x2, y2, false, carto, nullptr, false);
}

inline
void Point::remove ()
{
  if (prevOnLine)
    prevOnLine->nextOnLine = nextOnLine;
  if (nextOnLine)
    nextOnLine->prevOnLine = prevOnLine;

  prevOnLine = nullptr;
  nextOnLine = nullptr;
}

inline
void Point::initializeCarto (float xy, Carto* carto)
{
  Point* point1 = new Point (-xy, -xy, carto);
  Point* point2 = point1->addPoint (-xy, xy, false, carto, nullptr, false);
  Point* point3 = point2->addPoint (xy, xy, false, carto, nullptr, false);
  point3->addPoint (xy, -xy, false, carto, point1, false);
}

inline
float Point::x ()
{
  return x_;
}

inline
float Point::y ()
{
  return y_;
}

inline
void Point::update (date_t date)
{
  date_ = date;
}

inline
bool Point::up_to_date (date_t date)
{
  return (date == date_);
}

inline
bool Point::isObstacle ()
{
  return isObstacle_;
}

inline
bool Point::isVisited ()
{
  return isVisited_;
}

inline
void Point::setObstacle (bool isObstacle)
{
  isObstacle_ = isObstacle;
}

inline
void Point::link (Point* tonext, Carto* carto)
{
  assert (!(nextOnLine || tonext->prevOnLine));

  nextOnLine = tonext;
  tonext->prevOnLine = this;
  if (!eraseResidue (carto))
    distribute (this, tonext, carto);
}

inline
bool Point::ordAligned (Point* pt1, Point* pt2, float x3, float y3, Point* pt4)
{
  return ordAligned (pt1, pt2->x (), pt2->y (),
		     pt2->x () + pt4->x () - x3, pt2->y () + pt4->y () - y3);
}

inline
std::ostream& operator<< (std::ostream&, Point* pt)
{
  return std::cerr << (pt->prevOnLine ? "<" : " ")
		   << "(" << pt->x () << ", " << pt->y () << ")"
		   << (pt->nextOnLine ? ">" : " ");
}

#endif /* !POINT_HXX_ */
