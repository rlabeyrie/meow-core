/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef PGM_HXX_
# define PGM_HXX_

inline
bool Pgm::is_open ()
{
  return (file_.is_open ());
}

inline
void Pgm::close ()
{
  file_.close ();
}

#endif /* !PGM_HXX_ */
