/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef WAY_HXX_
# define WAY_HXX_

inline
float Way::x ()
{
  return scaled_x_ * carto_->getStep ();
}

inline
float Way::y ()
{
  return scaled_y_ * carto_->getStep ();
}

#endif /* !WAY_HXX_ */
