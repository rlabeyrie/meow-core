/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#include "cell.hh"
#include "carto.hh"

Cell::Cell ()
  : first_ (nullptr),
    unknownPoints_ (0)
{
}

Cell::~Cell ()
{
  while (first_)
  {
    Point* del = first_;
    first_ = first_->nextInCell;
    delete del;
  }
}

void Cell::add (Point* pt, Carto* carto)
{
  pt->nextInCell = first_;
  first_ = pt;
  if (!first_->isObstacle ())
    ++unknownPoints_;
  if (!carto->previouslySeen && carto->isCurrent (this))
    carto->previouslySeen = pt;
}

// Consider the robot sees from A to B with a margin CD: A-------[C-->B   D]
void Cell::trench (Carto* carto, Segment& nearestKept)
{
  Point* current = nullptr;
  Point* neighbor = nullptr;
  Point* prev = nullptr;
  Point* next = nullptr;

  carto->initCellCover (this, first_);
  bool nextNeighbor = false;
  while ((neighbor = carto->nextToSee ()))
  {
    if (neighbor != current)
    {
      nextNeighbor = false;
      current = neighbor;
    }
    else if (nextNeighbor)
      carto->seeNext ();
    nextNeighbor = !nextNeighbor;
    if (nextNeighbor)
    {
      prev = current;
      next = (neighbor = current->nextOnLine);
    }
    else
    {
      prev = (neighbor = current->prevOnLine);
      next = current;
    }

    float lambda = current->testNeighbor (carto, neighbor);

    if (lambda < nearestKept.lambda) // the segment [prev ; next] crosses AC
    {
      if (prev->isVisited ())
      {
	assert (next->isVisited ());
	if (lambda < 1.f - TOLERANCE)
	  carto->addInVisitedSegment (lambda, prev, next);
	else
	{
	  nearestKept.lambda = lambda;
	  nearestKept.prev = prev;
	  nearestKept.next = next;
	}
      }
      else
      {
	assert (!next->isVisited ());
	carto->addBorderSegment (lambda, prev, next);
      }
    }
  }
}

void Cell::colorize (unsigned* img, int display_size, int scale_factor,
		     int posx, int posy, float step, Carto* carto)
{
  Point* it = first_;
  while (it)
  {
    unsigned col = (it->isObstacle () ? KNOWN_COLOR_RGB : UNKNOWN_COLOR_RGB);
    float relx = it->x () / step;
    float rely = it->y () / step;
    float cellx = floor (relx);
    float celly = floor (rely);
    int x = posx + (1.f - relx + cellx) * scale_factor;
    int y = posy + (1.f - rely + celly) * scale_factor;
    if (it->prevOnLine && !it->prevOnLine->up_to_date (carto->date ()))
    {
      if (!it->prevOnLine->isObstacle ())
	col = UNKNOWN_COLOR_RGB;
      float relx2 = it->prevOnLine->x () / step;
      float rely2 = it->prevOnLine->y () / step;
      int x2 = posx + floor ((1.f - relx2 + cellx) * scale_factor);
      int y2 = posy + floor ((1.f - rely2 + celly) * scale_factor);
      carto->drawSegments (img, display_size, x, y, x2, y2, col);
    }
    if (it->nextOnLine && !it->nextOnLine->up_to_date (carto->date ()))
    {
      if (!it->nextOnLine->isObstacle ())
	col = UNKNOWN_COLOR_RGB;
      float relx2 = it->nextOnLine->x () / step;
      float rely2 = it->nextOnLine->y () / step;
      int x2 = posx + floor ((1.f - relx2 + cellx) * scale_factor);
      int y2 = posy + floor ((1.f - rely2 + celly) * scale_factor);
      carto->drawSegments (img, display_size, x, y, x2, y2, col);
    }
    if (it->prevOnLine || it->nextOnLine)
    {
      if (x < display_size && y < display_size)
      {
	int i = display_size * x + y;
	if (it->isObstacle ())
	{
	  if (img[i] == VOID_COLOR_RGB)
	    img[i] = col;
	}
	else if (img[i] != UNKNOWN_COLOR_RGB)
	  img[i] = col;
      }
      it->update (carto->date ());
    }
    it = it->nextInCell;
  }
}
