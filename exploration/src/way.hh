/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef WAY_HH_
# define WAY_HH_

class Cell; // forward dec

# include "carto.hh"

/// \brief Useful to look into all the cells crossed by a segment
class Way
{
public:
  /// \param step is the step defined in carto (as a private member)
  Way (Carto* carto);
  ~Way ();

  /// Place the current position at (beginx ; beginy) for the `hop'
  /// method, and the end to (beginx + vecx ; beginy + vecy).
  void set (float beginx, float beginy, float vecx, float vecy);

  /// Hop cell by cell from the last reached position in the direction
  /// of the end of the way (defined in the `way') while end is not reached.
  /// \return The current cell if it has not reached the end, nullptr otherwise.
  Cell* hop ();

  /// \return The current absolute abscissa on the way (see `hop ()')
  float x ();

  /// \return The current absolute ordinate on the way (see `hop ()')
  float y ();

private:
  Carto* carto_;

  int cellx_;
  int celly_;
  float inCellx_;
  float inCelly_;
  float vecx_;
  float vecy_;
  float scaled_x_;
  float scaled_y_;
};

# include "way.hxx"

#endif /* !WAY_HH_ */
