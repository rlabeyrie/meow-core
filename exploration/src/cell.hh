/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#ifndef CELL_HH_
# define CELL_HH_

# include <queue>

# include "point.hh"

class Carto; // forward dec
class Segment; // forward dec

/// \brief Describe a cell in a virtual grid placed over the map.
/// \details It contains the list of the points geographically located in the
/// virtual cell. It is used to manage close points and apply operations
/// in a neighborhood of a vector.
class Cell
{
public:
  Cell ();
  ~Cell ();

  /// Erase all on the way "View" * (1 - TOLERANCE) from the robot's position:
  /// open the relevant lines in this cell, and store all the segment crossed.
  void trench (Carto* carto, Segment& nearestKept);

  /// Add `pt' in the list of points of the cell.
  void add (Point* pt, Carto* carto);

  /// \return true if the cell does not contain any point.
  bool isEmpty ();

  /// \return The color of the cell, depending on the points it contains.
  unsigned color ();

  /// \return The color of the cell, depending on the points it contains.
  unsigned color_rgb ();

  /// Colorize `img' in a rectangle of dimensions width x height.
  void colorize (unsigned* img, int display_size, int scale_factor,
		 int posx, int posy, float step, Carto* carto);

  void removeFirst ();

  void delUnknown ();

private:
  void drawSegments (unsigned* img, int image_width, int x1, int y1,
		     int x2, int y2, unsigned col);

  // First element of the double linked list of all the "Point"s whose
  // absolute coordinates place them in this cell.
  Point* first_;

  // Number of elements in the list of "Point"s of the cell that
  // deserve to be visited because they are inherently uncertain.
  unsigned short unknownPoints_;
};

# include "cell.hxx"

#endif /* !CELL_HH_ */
