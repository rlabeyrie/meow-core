/**
 * \file main.cc
 * \brief Server for MEOW project.
 * \author BAILLY-FERRY Pierre
 * \version 0.1
 * \date 09/01/2012
 */

#include "carto.hh"
#include "SDL/SDL.h"
#include <stdlib.h>
#include "decision.hh"

#define DISP_SIZE 400

int main (int argc, char** argv)
{
  // SERVER CONNEXION
  if (argc == 1)
    {
      std::cout << "Waiting for a connexion, port: 4242\n" << std::endl;
      control_init(4242);
    }
  else
    {
      std::cout << "Waiting for a connexion, port: " << argv[1]
		<< "\n" << std::endl;
      control_init(atoi(argv[1]));
    }
  std::cout << "connected.\n" << std::endl;



  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    exit (1);
  SDL_Surface *screen = SDL_SetVideoMode(DISP_SIZE, DISP_SIZE, 32,
					 SDL_HWSURFACE);
  if (!screen)
  {
    std::cerr << "Error: SetVideoMode in main.cc" << std::endl;
    exit (1);
  }

  Carto* carto = new Carto (160.0f);
  Decision* decision = new Decision(carto);
  char choice = 'c';
  int scale = 9;
  int offset_x = 0;
  int offset_y = 0;

  while (choice != 'q')
  {
    SDL_LockSurface(screen);
    if (scale > 1)
      carto->display_magnify (static_cast<unsigned*> (screen->pixels),
			      DISP_SIZE, offset_x, offset_y, scale);
    else
      carto->display_reduce (static_cast<unsigned*> (screen->pixels),
			     DISP_SIZE, offset_x, offset_y, 2 - scale);
    SDL_Flip(screen);
    SDL_UnlockSurface(screen);
    
      
    SDL_Event event;
    choice = ' ';
    if (SDL_WaitEvent(&event))
      switch (event.type)
	{
	case SDL_KEYDOWN:
	  switch (event.key.keysym.sym)
	    {
	    case 269:
	      --scale;
	      choice = 'r';
	      break;
	    case 270:
	      ++scale;
	      choice = 'r';
	      break;
	    case SDLK_UP:
	      if (scale >= 1)
		offset_y += DISP_SIZE / scale / 2;
	      else
		offset_y += DISP_SIZE * (2 - scale) / 2;
	      choice = 'r';
	      break;
	    case SDLK_DOWN:
	      if (scale >= 1)
		offset_y -= DISP_SIZE / scale / 2;
	      else
		offset_y -= DISP_SIZE * (2 - scale) / 2;
	      choice = 'r';
	      break;
	    case SDLK_LEFT:
	      if (scale >= 1)
		offset_x += DISP_SIZE / scale / 2;
	      else
		offset_x += DISP_SIZE * (2 - scale) / 2;
	      choice = 'r';
	      break;
	    case SDLK_RIGHT:
	      if (scale >= 1)
		offset_x -= DISP_SIZE / scale / 2;
	      else
		offset_x -= DISP_SIZE * (2 - scale) / 2;
	      choice = 'r';
	      break;
	    default:
	      choice = 'c';
	    }
	  break;
	case SDL_QUIT:
	  choice = 'q';
	}

    decision->step();
  }
  delete carto;
  SDL_Quit();
  return 0;
}
