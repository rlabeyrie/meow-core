/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Hugues Lerebours Pigeonnière <hugues@lereboursp.net>
*/
#include "pgm.hh"
#include <sstream>

Pgm::Pgm (const char* filename, uint16_t w, uint16_t h)
  : file_ (filename, std::ios::out | std::ios::binary | std::ios::trunc)
{
  uint16_t maxv = 255;
  file_ << "P5 " << w << " " << h << " " << maxv << " ";
}

Pgm& Pgm::operator<< (uint8_t d)
{
  converter conv;
  conv.value = d;
  file_.write (&(conv.bin), 1);
  return *this;
}
