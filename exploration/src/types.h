#ifndef _ROBOT_TYPES_
# define _ROBOT_TYPES_

// concatenates two chars in one short
// remark: it concatenates rightside left to cheat endianness
# define CONCAT(type, cmd) (((cmd) << (sizeof(char) * 8)) + (type))
# define true 1
# define false 0

enum
{
	CMD_START = '[',
	CMD_STOP = ']'
};

/** \enum CommandType Defines the different action to request the robot
 */
typedef enum
{
	COMMAND_MF	= CONCAT('M', 'F'),		/*!< Move Forward */
	COMMAND_MB	= CONCAT('M', 'B'),		/*!< Move Backward */
	COMMAND_MR	= CONCAT('M', 'R'),		/*!< Move Right */
	COMMAND_ML	= CONCAT('M', 'L'),		/*!< Move Left */
	COMMAND_MS	= CONCAT('M', 'S'),		/*!< Move Stop */
	COMMAND_GS	= CONCAT('G', 'S'),		/*!< Get Speed */
	COMMAND_GA	= CONCAT('G', 'A'),		/*!< Get Angle */
	COMMAND_GD	= CONCAT('G', 'D'),		/*!< Get Distance */
	COMMAND_GV	= CONCAT('G', 'V'),		/*!< Get Vector */
	COMMAND_SS	= CONCAT('S', 'S'),		/*!< Set Speed */
	COMMAND_SA	= CONCAT('S', 'A')		/*!< Set Angle */
} CommandType;

typedef enum
{
	MOVE_FORWARD	= COMMAND_MF,
	MOVE_BACKWARD	= COMMAND_MB,
	MOVE_RIGHT		= COMMAND_MR,
	MOVE_LEFT		= COMMAND_ML
} Movement;


/** \struct Displacement represents the horizontal, vertical and angular
 * displacement of the robot.
 * x and y are in millimeters, a is degree
 */
typedef struct
{
	int16_t x, y, a;
} Displacement;

/** \struct Request Defines the structure of messages
 * send through the sockets
 */
typedef struct
{
	const char				start;
	const CommandType		type;
	union
	{
		int32_t				value;
		Displacement		displacement;
	};
	const char				stop;

} Command;

typedef struct
{
	int32_t				motor_speed;
	int32_t				turret_angle;
	int32_t				distance;
	Displacement		displacement;
} RobotState;

#endif // _ROBOT_TYPES_
