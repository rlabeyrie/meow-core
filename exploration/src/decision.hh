/**
 * \file decision.hh
 * \brief Take decision for the robot.
 * \author BAILLY-FERRY Pierre
 * \version 0.1
 * \date 09/01/2012
 */


#ifndef DECISION_HH_
# define DECISION_HH_

# include <stack>
# include <iostream>
# include <math.h>
# include "cell.hh"
# include "point.hh"
# include "carto.hh"
# include "types.h"


# define PI 3.14159265

extern "C"
{
  void control_init(int port);
  int control_get_distance (void);
  int control_get_turret_angle(void);
  char* control_get_image (void);
  // Displacement control_get_displacement(void);
  int control_get_motor_speed (void);
  void control_set_motor_speed (int speed);
  void control_set_turret_angle(int angle);
  void control_move (Movement mvt, int stop);
  void control_stop (void);
}


enum t_movementType {
  STRAIGHT,
  TURN_LEFT,
  TURN_RIGHT,
  DODGE_LEFT,
  DODGE_RIGHT
};

/// \brief Apply the actual movement for the robot and define the next one.
class Decision
{
private:
  // Bind the code with carto.cc, in order to put and retrieve information
  // in the map.
  Carto* carto_;

  // Remember the last return from IR sensor
  // IRtab_[0] is the extrem right ray
  // IRtab_[9] is the front ray
  // IRtab_[18] is the extrem left ray
  float IRtab_[19];

  // Define the next movement from the actual information.
  t_movementType nextMovementType_;

  /// Go forward.
  void straight();

  /// Turn to the left (90°).
  void turn_left();

  /// Turn to the right (90°).
  void turn_right();

  /// Turn to the left (90°) and fo forward until there is an object on the
  /// right of the robot. Finnaly, turn to the right (90°).
  void dodge_left();

  /// Turn to the right (90°) and fo forward until there is an object on the
  /// right of the robot. Finnaly, turn to the leftt (90°).
  void dodge_right();

  /// Define the correct movement for the next step.
  void next_step();

  /// Search is the robot is never passed on the right.
  bool right_interesting();

  /// Search is the robot is never passed on the left.
  bool left_interesting();

public:
  /// \param carto Bind the code with carto.cc, in order to put and retrieve 
  /// information in the map.
  Decision (Carto* carto);
  ~Decision ();

  /// apply the actual movement ad call next_step.
  void step();
};

#endif /* !DECISION_HH_ */
