DISTFILES=src Makefile AUTHORS README CMakeLists.txt configure config Doxyfile.in
PROJECT=meow-core
VERSION=0.1
DISTEXT=.tar.bz2

DISTNAME=$(PROJECT)-v$(VERSION)

all: build
	cd build && make

cmake: build
	cd build && cmake .. || rm -rf build

clean:
	cd build && make clean

distclean:
	rm -rf build

dist:
	mkdir $(DISTNAME) && \
	cp -r $(DISTFILES) $(DISTNAME) && \
	tar cvjf $(DISTNAME)$(DISTEXT) $(DISTNAME) && \
	rm -rf $(DISTNAME)

build: configure
	./configure

doc: build
	cd build && make doc

check: all
	cd build/src && ./meowcore