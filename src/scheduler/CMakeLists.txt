set(SCHEDULER_SOURCES
  scheduler.cc
  scheduler-load.cc
  scheduler-parser.cc
  )
#set(SCHEDULER_EXPORTED_SOURCES
#  scheduler-events.cc
#  )

set(SCHEDULER_LIBS ${CMAKE_THREAD_LIBS_INIT} dl)

add_library(scheduler ${SCHEDULER_SOURCES})
#add_library(scheduler-exported ${SCHEDULER_EXPORTED_SOURCES})

target_link_libraries(scheduler ${SCHEDULER_LIBS})
