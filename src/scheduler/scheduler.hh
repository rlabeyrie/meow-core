/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file scheduler.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Definition of the main class of the scheduler. This is the
 *        main class of the project, which will instanciate everything
 *        else.
 */

#ifndef SCHEDULER_HH_
# define SCHEDULER_HH_

# include <deque>
# include <map>
# include <string>
# include <pthread.h>

# include "../events/event.hh"

// forward declarations
class Module;
class CriticalModule;

/*!
 * \class Scheduler
 *
 * \brief Main class of the project. Instanciates everything else.
 *
 * This class instanciates and schedules all the modules and events of
 * the project.
 */
class Scheduler
{
public:
  /*!
   * Only constructor of the class. Instanciates and initializes
   * attributes.
   */
  Scheduler ();

  /*!
   * Destructor of the class. Frees memory used by attributes
   * instanciated on heap.
   */
  ~Scheduler ();

// public initialization function
public:
  /*!
   * Main function. This is called by the main () function of the
   * program, and will call every other function needed for
   * initialization, as well as the main loop.
   */
  void start ();

// public util functions
public:
  /*!
   * \brief Pushes an event on the scheduler's events stack.
   *
   * This method is meant to be called by modules. This is a
   * thread-safe method and must be used with parcimony as there might
   * be a lot of mutex-lock involved. Try to discretize as much as
   * possible your event stream.
   *
   * Any event pushed this way will trigger any list of modules
   * associated to this event class/ID.
   *
   * \param event Event to be pushed on the stack.
   *
   * \see Module
   * \see Event
   * \see add_trigger
   */
  void push_event (Event* event);

  /*!
   * \brief Loads the module located at the path "path".
   *
   * \param path Path where the desired module shared library is
   *             located.
   *
   * \return An instance of the module class that has just been
   *         loaded.
   *
   * \see Module
   */
  Module* load_module (const char* path);

  /*!
   * \brief Loads the event located at the path "path".
   *
   * \param path Path where the desired event shared library is
   *             located.
   *
   * \return An instance of the event class that has just been
   *         loaded.
   *
   * \see Event
   */
  Event::id_type load_event (const char* path);

  /*!
   * \brief Adds a trigger on a given event for a given list of
   * modules.
   *
   * When an event corresponding to the id "id" is pushed, all the
   * modules in the "modules" list are sequencially called.
   *
   * \param id Id of the event class which will trigger the modules.
   *
   * \param modules List of modules to be called when the event is
   *                pushed.
   *
   * \see Event::id_type
   * \see Module::receive
   */
  void add_trigger (Event::id_type id, std::deque<Module*> modules);

// intern initialization functions
private:
  /*!
   * \brief Instanciates the critical modules' threads.
   *
   * Every critical module must run continuously in its own thread, in
   * order to pull environment-based informations regularly. These
   * threads are instanciated in this function, after the modules are
   * loaded.
   */
  void initiate_critical_modules ();

  /*!
   * \brief Parses the script config file and loads the corresponding
   *        modules and events.
   *
   * A way to add triggers between events and modules is to write a
   * configuration file in a certain script language. This is parsed
   * in this function (TODO).
   */
  void parse_config_file ();

private:
  /*!
   * Double-ended list containing all the modules loaded by the scheduler.
   *
   * \see Scheduler::_critical_modules
   * \see Scheduler::_regular_modules
   * \see Scheduler::load_modules
   */
  std::deque<Module*> _modules;

  /*!
   * Double-ended list containing all the critical modules loaded by
   * the scheduler. Those are also contained in the general modules
   * list.
   *
   * \see Scheduler::_modules
   */
  std::deque<CriticalModule*> _critical_modules;

  /*!
   * Double-ended list containing all the regular (i.e. non-critical)
   * modules loaded by the scheduler. Those are also contained in the
   * general modules list.
   *
   * \see Scheduler::_modules
   * \see Scheduler::initiate_critical_modules
   */
  std::deque<Module*> _regular_modules;

  /*!
   * Double-ended list containing all the threads instanciated for the
   * critical modules, in order for them to be joined and closed
   * properly.
   */
  std::deque<pthread_t*> _threads;

  /*!
   * Double-ended queue containing pointers to a boolean qualifying
   * the state of a thread: alive or dead.
   *
   * \see Scheduler::_threads
   */
  std::deque<bool*> _threads_status;

  /*!
   * Map containing all the triggers associating a list of modules to
   * an event type.
   *
   * \see Event::id_type
   * \see Module
   * \see Scheduler::add_trigger
   */
  std::map<Event::id_type, std::deque<Module*>> _triggers;

  /*!
   * \brief Last id attributed to an event class. Incremented each
   *        time.
   *
   * In order to prevent conflicts between user's event classes, an
   * unique ID must be attributed at run-time to each class. This
   * attribute permits to keep trace of the last ID attributed.
   */
  Event::id_type _last_event_id;

  /*!
   * \brief Events queue. List of events waiting to be forwarded to
   *        modules.
   *
   * \see push_event
   * \see Module::receive
   * \see Event
   */
  std::deque<Event*> _events_queue;

  /*!
   * \brief Mutex locking the accesses to the events queue.
   *
   * This mutex is only locked when inserting or deleting an event
   * from the queue. It is not locked while the scheduler pulls events
   * and dispatches them to modules, in order to reduce the critical
   * section.
   */
  pthread_mutex_t _mutex_queue;

  /*!
   * \brief PThread condition, releasing the scheduler if he waits for
   *        the event queue to be filled.
   *
   * The scheduler waits on this condition if the events queue is
   * empty. Every time an event is pushed, a signal on this condition
   * is sent to potentially release the scheduler.
   */
  pthread_cond_t _cond_queue;

  /*!
   * \brief Mutex locking the accesses to the tty (read/write calls)
   *
   * This mutex must be locked when reading from STDIN, or writing on
   * STDOUT and STDERR when the program is running under a
   * tty. Otherwise, the visual output might be stochastic.
   *
   * If the output is only intended for log purposes, you may avoid
   * using this mutex.
   */
  pthread_mutex_t _mutex_tty;
};

# include "scheduler.hxx"

#endif /* !SCHEDULER_HH_ */
