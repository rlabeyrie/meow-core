#include "scheduler.hh"
#include <iostream>

inline
void Scheduler::push_event (Event* event)
{
  pthread_mutex_lock (&_mutex_queue);

  _events_queue.push_back (event);

  pthread_mutex_unlock (&_mutex_queue);
  pthread_cond_signal (&_cond_queue);
 }
