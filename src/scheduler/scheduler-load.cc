/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file scheduler-load.cc
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Implementation of the run-time module and event loading
 *        functionnality.
 */

#include "scheduler.hh"
#include "../modules/module.hh"

#include <dlfcn.h>
#include <iostream>

void (*maker_event) (Event::id_type) = NULL;
Module* (*maker_module) () = NULL;

Event::id_type Scheduler::load_event (const char* path)
{
  void* lib = dlopen (path, RTLD_LAZY);

  if (lib)
  {
    std::cout << "- Loading event `" << path << "'" << std::endl;
    _last_event_id += 1;
    maker_event (_last_event_id);
  }
  else
  {
    std::cerr << "- Error loading event `" << path << "': "
	      << dlerror () << std::endl;
  }
  return _last_event_id;
}

Module* Scheduler::load_module (const char* path)
{
  Module* module = NULL;
  Module* (*old_maker) () = maker_module;
  void* lib = dlopen (path, RTLD_LAZY);

  if (lib && maker_module != old_maker)
  {
    std::cout << "- Loading module `" << path << "'" << std::endl;
    module = maker_module ();
    module->scheduler_ = this;
    _modules.push_back (module);
    if (dynamic_cast<CriticalModule*> (module))
      _critical_modules.push_back ((CriticalModule*) module);
    else
      _regular_modules.push_back (module);
  }
  else
  {
    std::cerr << "- Error loading module `" << path << "': "
	      << dlerror() << std::endl;
  }
  return module;
}

void Scheduler::add_trigger (Event::id_type id, std::deque<Module*> seq)
{
  _triggers[id] = seq;
}
