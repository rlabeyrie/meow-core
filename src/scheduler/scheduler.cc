/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file scheduler.cc
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Implementation of the basic methods of the scheduler.
 */

#include "scheduler.hh"
#include "../modules/module.hh"

#include <iostream>

namespace
{
/*!
 * \struct ThreadArg
 *
 * \brief Local structure used for passing several arguments at a
 *        thread's instanciation.
 */
  struct ThreadArg
  {
    CriticalModule* module;
    bool* alive;
  };

/*!
 * \fn void* thread_entry_point (void* argspack)
 *
 * \brief Called when a module thread is instanciated.
 *
 * \param argspack a ThreadArg* containing the module and a pointer to
 *                 the stop boolean.
 */
  void* thread_entry_point (void* argspack)
  {
    ThreadArg* arg = (ThreadArg*) argspack;

    arg->module->start (arg->alive);
    delete arg;

    return NULL;
  }
} // anonymous

Scheduler::Scheduler ()
  : _last_event_id (-1),
    _mutex_queue (PTHREAD_MUTEX_INITIALIZER),
    _cond_queue (PTHREAD_COND_INITIALIZER),
    _mutex_tty (PTHREAD_MUTEX_INITIALIZER)
{
}

Scheduler::~Scheduler ()
{
  std::cout << "Safe exit started." << std::endl;

  std::cout << "Terminating threads..." << std::endl;

  for (auto alive : _threads_status)
    *alive = false;
  for (auto thread : _threads)
    pthread_join (*thread, NULL);
  for (auto thread : _threads)
    delete thread;

  std::cout << "Cleaning modules...";

  for (auto module : _modules)
    delete module;

  std::cout << " done." << std::endl;
}

void Scheduler::parse_config_file ()
{
  std::cout << "Parsing config file..." << std::endl;

  auto event = load_event ("events/dummy/dummyevent.so");
  std::deque<Module*> sequence = {
    load_module ("modules/regulardummy/regulardummy.so"),
    load_module ("modules/criticaldummy/criticaldummy.so")
  };
  add_trigger(event, sequence);
}

void Scheduler::initiate_critical_modules ()
{
  std::cout << "Instanciating threads for critical modules...";

  for (auto module : _critical_modules)
  {
    pthread_t* thread = new pthread_t ();
    bool* alive = new bool (true);

    _threads.push_back (thread);
    _threads_status.push_back (alive);
    pthread_create (thread, NULL, thread_entry_point, new ThreadArg {module, alive});
  }

  std::cout << " done." << std::endl;
}

void Scheduler::start ()
{
  std::cout << "MEOW Core platform starting." << std::endl;
  parse_config_file ();
  initiate_critical_modules ();

  while (true)
  {
    pthread_mutex_lock (&_mutex_queue);
    while (_events_queue.empty ())
      pthread_cond_wait (&_cond_queue, &_mutex_queue);

    Event* event = _events_queue.front ();

    for (auto pair : _triggers)
    {
      if (pair.first == event->get_id ())
	for (auto module : pair.second)
	{
	  module->receive (event);
	}
    }

    delete event;
    _events_queue.pop_front ();

    pthread_mutex_unlock (&_mutex_queue);
  }
}
