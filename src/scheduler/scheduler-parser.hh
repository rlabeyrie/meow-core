/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file scheduler-parser.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Definition of the config script parser class. This class
 *        interprets the user configuration of the robot and calls the
 *        scheduler's loader for appropriate modules and events.
 */

#ifndef SCHEDULER_PARSER_HH_
# define SCHEDULER_PARSER_HH_

/*!
 * \class SchedulerParser
 *
 * \brief The main class of the scheduler parser. Must only be
 *        instanciated by the scheduler itself.
 */
class SchedulerParser
{
};

#endif /* !SCHEDULER_PARSER_HH_ */
