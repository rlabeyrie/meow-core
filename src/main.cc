/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file main.cc
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Main file, containing the \link main \endlink
 *        function. Instanciates the scheduler.
 *
 * The scheduler is instanciated, then its main loop started. Nothing
 * more.
 */

#include "scheduler/scheduler.hh"

int main ()
{
  Scheduler scheduler =  Scheduler ();

  scheduler.start ();

  return 0;
}
