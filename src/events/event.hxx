/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file event.hxx
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Back-end of the Event interface. Mostly ugly hacks allowing
 *        run-time loading.
 */

#ifndef EVENT_HXX_
# define EVENT_HXX_

# undef MEOW_EVENT_DEF
# define MEOW_EVENT_DEF(NAME)			\
  public:					\
  static id_type _id;				\
  virtual void set_id (id_type);		\
  virtual id_type get_id ();

# undef MEOW_EVENT_IMP
# define MEOW_EVENT_IMP(NAME)			\
  Event::id_type NAME::_id = -1;		\
  void NAME::set_id (Event::id_type id)		\
  {						\
    _id = id;					\
  }						\
  Event::id_type NAME::get_id ()		\
  {						\
    return _id;					\
  }						\
  extern "C"					\
  {						\
    void maker ## NAME (Event::id_type id)	\
    {						\
      NAME event;				\
      event.set_id (id);			\
    }						\
    class Register				\
    {						\
      public:					\
      Register ()				\
      {						\
	maker_event = maker ## NAME;		\
      }						\
    };						\
    Register r;					\
  }

extern void (*maker_event) (Event::id_type);

#endif /* !EVENT_HXX_ */
