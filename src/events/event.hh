/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file event.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Definition of the abstract Event class. This is an User
 *        Interface, read attentively every comment on this file.
 *
 * Every event must be compiled in a dedicated shared library.
 */

#ifndef EVENT_HH_
# define EVENT_HH_

/*!
 * \def MEOW_EVENT_DEF(NAME)
 *
 * \brief Necessary for the definition of a new Event class.
 *
 * Must be used at the beginning of a new event class definition
 * (inside the class).
 *
 * Ugly hack. Allows to keep track of a static field defining the
 * unique ID of the class, defined at run-time by the scheduler.
 *
 * \param NAME Name of the class.
 *
 * \see MEOW_EVENT_IMP
 */
# define MEOW_EVENT_DEF(NAME)

/*!
 * \def MEOW_EVENT_IMP(NAME)
 *
 * \brief Necessary for the implementation of a new Event class.
 *
 * Ugly hack. Allows to:
 *   - keep track of a static field defining the unique ID of the
 *     class, defined at run-time by the scheduler.
 *   - write the interface allowing the scheduler to load the library
 *
 * \param NAME Name of the class.
 *
 * \see MEOW_EVENT_DEF
 */
# define MEOW_EVENT_IMP(NAME)

/*!
 * \class Event
 *
 * \brief Abstract class defining the nature of an event. This is
 *        pushed and received by modules.
 *
 * Every event class must inherit from this class, and must be
 * compiled in a separate shared library. This library will be loaded
 * by the scheduler at run-time.
 *
 * \see Scheduler
 * \see Scheduler::load_event
 * \see Module::receive
 */
class Event
{
public:
  /*!
   * \brief Defines the type of events' IDs. Currently integers.
   */
  typedef int id_type;

  /*!
   * The scheduler needs to be intimate with the Event class.
   */
  friend class Scheduler;

  /*!
   * Sets the static field "id" of the concrete class.
   *
   * \param id The newly defined ID of the class.
   */
  virtual void set_id (id_type id) = 0;

  /*!
   * Gets the unique id of the concrete event class.
   *
   * \return The id of the class.
   */
  virtual id_type get_id () = 0;
};

# include "event.hxx"

#endif /* !EVENT_HH_ */
