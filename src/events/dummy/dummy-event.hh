/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file dummy-event.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Definition of the dummy event class. This is an example.
 */

#ifndef DUMMY_EVENT_HH_
# define DUMMY_EVENT_HH_

# include "../event.hh"

/*!
 * As the name says, a dummy event, just to show how it is done.
 */
class DummyEvent : public Event
{
  MEOW_EVENT_DEF (DummyEvent);
};

#endif /* !DUMMY_EVENT_HH_ */
