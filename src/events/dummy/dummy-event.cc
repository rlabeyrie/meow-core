/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file dummy-event.cc
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Implementation of the dummy event class. Nothing much, just
 *        showing how to implement an event.
 */

#include "dummy-event.hh"

MEOW_EVENT_IMP (DummyEvent);
