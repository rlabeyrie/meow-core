/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file module.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Interface for the user. Definition of the module class,
 *        necessary to create a new module.
 *
 * You need every piece of information contained in this file, if not
 * indicated otherwise. Every module must be compiled in an
 * independant shared library and inherit from the Module class.
 */

#ifndef MODULE_HH_
# define MODULE_HH_

# include "../events/event.hh"

# include <vector>

/*!
 * \brief Definition of a new module.
 *
 * Necessary to define a new Module. Use in the class definition.
 *
 * \param NAME Name of the new module class.
 *
 * \see regular-dummy.hh
 */
# define MEOW_MODULE_DEF(NAME)

/*!
 * \brief Implementation of a new module.
 *
 * Necessary to implement a new Module. Use in the class
 * implementation.
 *
 * \param NAME Name of the new module class.
 *
 * \see regular-dummy.cc
 */
# define MEOW_MODULE_IMP(NAME)

/*!
 * \brief List of all the events the new module must know.
 *
 * Necessary to dynamically dispatch the events the module wants to
 * receive. Every event type awaited must be listed here. Plus, the
 * \link Module::receive \endlink method must be overloaded with the
 * related event type.
 *
 * \param NAME Name of the new module class.
 * \param EVENTS Variadic list of the events to be linked.
 *
 * \see regular-dummy.cc
 * \see Event
 */
# define MEOW_BIND_EVENTS(NAME, EVENTS...)

// forward declaration
class Scheduler;

/*!
 * \class Module
 *
 * \brief Abstract class defining the interface between user-defined
 *        modules and the scheduler.
 *
 * Every new module must inherit from this class, and use the
 * above-defined macros.
 */
class Module
{
public:
  /*!
   * \brief This method is called when an event is pushed and a
   *        trigger binding this event to this module exists.
   *
   * You must overload this method for every concrete event type that
   * you have binded with the \link MEOW_BIND_EVENTS \endlink macro.
   *
   * \param event Instance of the pushed event. This is usuall called
   *              by the scheduler.
   *
   * \see Event
   */
  virtual void receive (Event* event) = 0;

protected:
  /*!
   * \brief Pushes an event on the scheduler's events stack.
   *
   * This function allows any module to communicate with any other one
   * through events. You can push one of any type, given it inherits
   * from the \link Event \endlink class.
   *
   * Note that the event stack may be accessed by several threads at
   * once, and thus is mutex-restricted. You should use this method
   * discretely.
   *
   * \param event Event to be pushed on the scheduler's stack.
   *
   * \see Event
   */
  void push (Event* event);

protected:
  /*!
   * \brief Back-end related. Do NOT modify it.
   *
   * \internal
   *
   * Back-end related. Events to which the concrete module is
   * binded. Defined at run-time as the events are pushed.
   */
  std::vector<int> _events_id;

  /*!
   * \brief Back-end related. Useless for end-users.
   *
   * \internal
   *
   * Back-end related. This class does all the dynamic dispatch,
   * thanks to variadic templates (sexy and horrible at the same
   * time).
   */
  template <typename... T> friend class DispatchEvent;

private:
  friend class Scheduler;
  /*!
   * Initialized by the scheduler itself. Used for the \link push
   * \endlink method.
   *
   * \see push
   */
  Scheduler* scheduler_;
};


/*!
 * \class CriticalModule
 *
 * \brief Abstract class defining the interface between user-defined
 *        critical modules and the scheduler.
 *
 * Has the same features as a regular Module, plus a permanent thread
 * launched at the start of the platform. Every critical module should
 * inherit from this class, rather than launch its own thread.
 *
 * Note that CriticalModule already inherits from the Module class,
 * thus you must not inherit from the Module class if you inherit from
 * CriticalModule.
 *
 * \see Module
 */
class CriticalModule : public Module
{
public:
  /*!
   * \brief Routine called at the start of the critical module's
   *        thread.
   *
   * \param alive "true" if the thread must continue, "false"
   *              otherwise.
   */
  virtual void start (bool* alive) = 0;
};

# include "module.hxx"

#endif /* !MODULE_HH_ */
