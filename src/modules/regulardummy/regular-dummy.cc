/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file regular-dummy.cc
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Implementation of the regular dummy module class. Nothing
 *        much, just showing how to implement a module.
 */

#include "regular-dummy.hh"
#include "../../events/dummy/dummy-event.hh"

#include <iostream>

MEOW_MODULE_IMP (RegularDummy);
MEOW_BIND_EVENTS (RegularDummy, DummyEvent);

void RegularDummy::receive (DummyEvent*)
{
  std::cout << "HELL YEAH" << std::endl;
}
