/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file regular-dummy.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Definition of the regular dummy module class. This is an
 *        example.
 */

#ifndef REGULAR_DUMMY_HH_
# define REGULAR_DUMMY_HH_

# include "../module.hh"

// forward decs
class DummyEvent;

/*!
 * As the name says, a dummy module, just to show how it is done.
 */
class RegularDummy : public Module
{
  MEOW_MODULE_DEF(RegularDummy);
public:
  /*!
   * This is an overloaded definition of the \link receive \endlink
   * method. Only one here because we only bind the module with one
   * event.
   */
  void receive (DummyEvent*);
};

#endif /* !CRITICAL_DUMMY_HH_ */
