/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file module.hxx
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Back-end of the module interface. No need to read this if
 *        you are not developping Meow.
 *
 * Ugly hacks based on variadic templates (hurray for C++11), allowing
 * dynamic dispatch of events (sugar for the user), and some stuff
 * permitting run-time loading of the library.
 */

#ifndef MODULE_HXX_
# define MODULE_HXX_

# include "../scheduler/scheduler.hh"

# undef MEOW_MODULE_DEF
# define MEOW_MODULE_DEF(NAME)			\
  public:					\
  virtual void receive (Event*);

# undef MEOW_MODULE_IMP
# define MEOW_MODULE_IMP(NAME)			\
  extern "C"					\
  {						\
    Module* maker ## NAME ()			\
    {						\
      return new NAME ();			\
    }						\
    class Register				\
    {						\
      public:					\
      Register ()				\
      {						\
	maker_module = maker ## NAME;		\
      }						\
    };						\
    Register r;					\
  }

# undef MEOW_BIND_EVENTS
# define MEOW_BIND_EVENTS(NAME, EVENTS...)		\
  void NAME::receive (Event* event)			\
  {							\
    DispatchEvent<NAME, EVENTS>::dispatch (this, event);	\
  }

template <typename... T>
struct DispatchEvent;

template <typename M, typename CAR, typename... CDR>
struct DispatchEvent <M, CAR, CDR...>
{
  static const int count = 1 + DispatchEvent<M, CDR...>::count;
  static void dispatch (Module* mod, Event* event)
  {
    int evid = event->get_id ();
    int id = -1;

    if (mod->_events_id.size () > (unsigned) count)
      id = mod->_events_id[count];
    if (id == evid)
      ((M*)(mod))->receive ((CAR*) event);
    else if (id > 0)
      DispatchEvent<M, CDR...>::dispatch (mod, event);
    else
    {
      if (dynamic_cast<CAR*> (event) != 0)
      {
	mod->_events_id.push_back (evid);
	((M*)(mod))->receive ((CAR*) event);
      }
      else
	DispatchEvent<M, CDR...>::dispatch (mod, event);
    }
  }
};

template <typename M>
struct DispatchEvent <M>
{
  static const int count = -1;
  static void dispatch (Module*, Event*)
  {
    std::cout << "tamere" << std::endl;
  }
};

extern Module* (*maker_module) ();

inline
void Module::push (Event* event)
{
  scheduler_->push_event (event);
}

#endif /* !MODULE_HXX_ */
