/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file critical-dummy.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Implementation of the critical dummy module class. This is
 *        an example.
 */

#include "critical-dummy.hh"
#include "../../events/dummy/dummy-event.hh"

#include <iostream>
#include <ctime>

MEOW_MODULE_IMP (CriticalDummy);
MEOW_BIND_EVENTS (CriticalDummy, DummyEvent);

void CriticalDummy::receive (DummyEvent*)
{
}

void CriticalDummy::start (bool* alive)
{
  while (*alive)
  {
    std::cout << "CriticalDummy just spamming randomly..." << std::endl;
    push (new DummyEvent ());
    sleep (1);
  }
}
