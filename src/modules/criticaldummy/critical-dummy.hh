/*
 * MEOW Project
 * EPITA GISTR - SCIA 2012
 * Rémi Labeyrie - rlabeyrie@gmail.com
 */

/*!
 * \file critical-dummy.hh
 *
 * \author Rémi Labeyrie
 * \email rlabeyrie@gmail.com
 *
 * \brief Definition of the critical dummy module class. This is an
 *        example.
 */

#ifndef CRITICAL_DUMMY_HH_
# define CRITICAL_DUMMY_HH_

# include "../module.hh"

// forward decs
class DummyEvent;

/*!
 * As the name says, a dummy module, just to show how it is done.
 */
class CriticalDummy : public CriticalModule
{
  MEOW_MODULE_DEF(CriticalDummy);

public:
  /*!
   * This is the main routine, called after the instanciation of the
   * critical module's thread and must be constantly running, unless
   * the given argument equals "false".
   *
   * \param alive "true" if the thread must continue, "false"
   *              otherwise.
   */
  virtual void start (bool* alive);

  /*!
   * This is an overloaded definition of the \link receive \endlink
   * method. Only one here because we only bind the module with one
   * event.
   */
  void receive (DummyEvent*);
};

#endif /* !CRITICAL_DUMMY_HH_ */
